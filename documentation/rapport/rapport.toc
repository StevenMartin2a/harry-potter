\babel@toc {french}{}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Conception}{3}
\contentsline {subsection}{\numberline {2.1}Premi\IeC {\`e}re id\IeC {\'e}e}{3}
\contentsline {subsection}{\numberline {2.2}Charte graphique}{3}
\contentsline {subsection}{\numberline {2.3}Wireframe}{3}
\contentsline {section}{\numberline {3}Cr\IeC {\'e}ation du site}{3}
\contentsline {subsection}{\numberline {3.1}Navigation}{3}
\contentsline {subsubsection}{\numberline {3.1.1}Navigation entre les pages}{3}
\contentsline {subsubsection}{\numberline {3.1.2}Navigation vers les sources des images et du texte}{3}
\contentsline {subsection}{\numberline {3.2}Structure HTML}{4}
\contentsline {subsubsection}{\numberline {3.2.1}sch\IeC {\'e}ma}{4}
\contentsline {subsection}{\numberline {3.3}CSS}{4}
\contentsline {subsubsection}{\numberline {3.3.1}Flexbox}{4}
\contentsline {subsubsection}{\numberline {3.3.2}Grid}{4}
\contentsline {subsection}{\numberline {3.4}Page et contenu / Arborescence}{5}
\contentsline {section}{\numberline {4}Modification li\IeC {\'e} \IeC {\`a} la premi\IeC {\`e}re \IeC {\'e}valuation}{5}
\contentsline {section}{\numberline {5}Probl\IeC {\`e}me / R\IeC {\'e}solution}{5}
\contentsline {section}{\numberline {6}Commentaire rendu final}{5}
\contentsline {section}{\numberline {7}Am\IeC {\'e}lioration possible}{5}
